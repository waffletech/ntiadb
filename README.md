# ntiadb

This is a tool to parse the FOIA release PDF of the Government Master File into a database, and then a web interface
to query and explore that database. The goal is to provide an experience similar to (...but more performant) the FCC
ULS for NTIA allocations, which have historically been very difficult to work with because of being documented mostly
in the form of a 15,000 page PDF.

## Current status
Parsing of the PDF works correctly, although it could use an enhancement to handle continuation pages on its own and
to more intelligently detect pages where the pdfminer extraction of the text left spurious lines at the beginning. In
general the pdfminer part could use work, because there are always spurious lines at the end of pages. This tool ignores
them but it's annoying.

## Creating the database

First, you will need a FOIA release PDF of the NTIA Government Master File. This document, which has occasionally been
requested by FOIA by various people to get more up to date versions, appears to be the output of s/370-type software
sent to a virtual printer. It's a PDF of 132-column-wide fixed-width output. Unfortunately it's harder to work with
than that would imply because of the vertical continuation of multi-value fields, and presentation of the comments and
remarks side-by-side.

Here are the steps to use the tool:

1. `poetry init` to get dependencies etc.
2. Edit the instance-specific config.toml (instance/config.toml) to specify the correct path to the NTIA PDF and a
   directory to write the intermediate per-page output.
3. Run the extract-pages CLI subcommand, e.g. `poetry run flask extract-pages`. This takes ~15 minutes.
4. Clean up the intermediate page files so they only contain record pages. You will need to remove leader and trailer
   pages (like the FOIA letter if attached and the classification summary end page). Other types of unusual/erroneous
   pages should be handled correctly by the parser: pages with no records at all, pages containing only a continuation
   of the remarks for a record on the previous page, and pages with extraneous lines before the classification header
   and/or after the classification footer. Pages with extraneous non-record lines between the classification
   header and footer (in other words, within the actual page text) will stop the parser and will need to be manually
   corrected.
5. Run the parse-pages subcommand, e.g. `poetry run flask run parse-pages`. This takes ~30 minutes. If the process
   hits an exception, investigate the page that caused the exception to fix any issues, then drop the database and try
   parse-pages again. This isn't the most elegant way to work through problems, but parsing the PDF is pretty much a
   one-time process after you spent weeks waiting for a FOIA response, so it's not that bad.

## Using the web interface

TBD...