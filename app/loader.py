from pathlib import Path
import re
from io import StringIO
from natsort import natsorted

from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage

from app import app, models

def field(string, start, end):
    if len(string) > end:
        return string[start:end].strip()
    elif len(string) > start:
        return string[start:].strip()
    else:
        return ""


def normalize_frequency(freq):
    # This is a bit clumsy, but frequencies always come with 9 digits, so we treat those as a big integer and then
    # multiple for powers if we need to.
    numeric = int(freq[1:].replace(".", ""))
    if freq[0] == "K":
        return numeric / 1000
    elif freq[0] == "M":
        return numeric
    elif freq[0] == "G":
        return numeric * 1000
    else:
        app.logger.error(f"Unknown frequency unit on {freq}")


def normalize_power(pwr):
    if not pwr:
        return None

    numeric = float(pwr[1:])
    if pwr[0] == "W":
        return numeric
    if pwr[0] == "K":
        return numeric * 1000
    if pwr[0] == "M":
        return numeric * 1000000
    else:
        app.logger.error(f"Unknown power unit on {pwr}")


def normalize_coordinates(cs):
    if not cs:
        return None, None

    app.logger.debug(f"Parsing coordinates {cs}...")

    lat_float = (float(cs[0:2]) + float(cs[2:4]) / 60 + float(cs[4:6]) / (60 * 60)) * (-1 if cs[6:7] == 'S' else 1)
    lon_float = (float(cs[7:10]) + float(cs[10:12]) / 60 + float(cs[12:14]) / (60 * 60)) * (-1 if cs[14:15] == 'W' else 1)
    app.logger.debug(f"lat card: {cs[6:7]} lon card: {cs[14:15]}")

    app.logger.debug(f"    ...got {lat_float}, {lon_float}")
    return lat_float, lon_float


def parse_record_begin(line):
    app.logger.debug(f"Parsing as allocation beginning: {line}")
    allocation = models.Allocation()

    allocation.cdd       = field(line, 4, 15)
    allocation.frequency = normalize_frequency(field(line, 15, 28))
    allocation.bin       = field(line, 28, 31)
    allocation.serial    = field(line, 31, 44)
    allocation.mbr       = field(line, 44, 48)
    allocation.foi       = field(line, 48, 52)
    allocation.bureau    = field(line, 52, 57)
    allocation.net       = field(line, 57, 63)
                         # Gap here due to repeating fields handled below
    allocation.aud       = field(line, 112, 119)
    allocation.rvd       = field(line, 119, 126)
    allocation.exd       = field(line, 126, 150)
    allocation.save()

    # This one is kind of weird because it's just going to be the first emission
    first_emission = models.Emission()
    first_emission.station_class = field(line, 63, 70)
    first_emission.designator    = field(line, 70, 85)
    first_emission.power         = normalize_power(field(line, 85, 98))

    first_nts = models.Nts()
    first_nts.nts                = field(line, 98, 103)

    first_aus = models.Aus()
    first_aus.aus                = field(line, 103, 112)

    # link up the beginnings of multi-value fields...
    first_emission.allocation = allocation
    first_emission.save()
    first_nts.allocation = allocation
    first_nts.save()
    first_aus.allocation = allocation
    first_aus.save()

    return allocation


def parse_allocation_continuation(line, allocation):
    app.logger.debug(f"Parsing as allocation continuation: {line}")
    emission_class = field(line, 63, 70)
    emission_desi  = field(line, 70, 85)
    emission_pwr   = field(line, 85, 98)
    if emission_class and emission_desi and emission_pwr:
        new_emission = models.Emission()
        new_emission.allocation = allocation
        new_emission.station_class = emission_class
        new_emission.designator = emission_desi
        new_emission.power = normalize_power(emission_pwr)
        new_emission.save()
    elif emission_class or emission_desi or emission_pwr:
        app.logger.error(f"Incomplete emission record: {line}")

    nts = field(line, 98, 103)
    if nts:
        new_nts = models.Nts()
        new_nts.allocation = allocation
        new_nts.nts = nts
        new_nts.save()

    aus = field(line, 103, 112)
    if aus:
        new_aus = models.Aus()
        new_aus.allocation = allocation
        new_aus.aus = aus
        new_aus.save()


def parse_site(line, allocation, role):
    app.logger.debug(f"Parsing as site: {line}")
    new_site = models.Site()
    new_site.role              = role
    new_site.allocation        = allocation
    new_site.state             = field(line, 3, 8)
    new_site.location          = field(line, 8, 33)
    new_site.rc                = field(line, 33, 42)
    new_site.lat, new_site.lon = normalize_coordinates(field(line, 42, 58))
    new_site.cl                = field(line, 58, 67)
    new_site.antenna           = field(line, 67, 92)
    new_site.polarization      = field(line, 92, 96)
    new_site.azimuth           = field(line, 96, 100)
    new_site.spd               = field(line, 100, 105)
    new_site.tme               = field(line, 105, 110)
    new_site.ici               = field(line, 110, 114)
    new_site.msd               = field(line, 114, 150)
    new_site.save()

    if new_site.lat and new_site.lon:
        app.logger.debug(f"Inserting spatialite point for {new_site.lat},{new_site.lon}")
        models.db.execute_sql(f"UPDATE site SET Geometry = ST_PointFromText('POINT ({new_site.lon} {new_site.lat})', 4326) WHERE id = {new_site.id}")


def parse_comment_line(line, allocation):
    app.logger.debug(f"Parsing as comment line: {line}")
    comment_part = field(line, 3, 81)
    if comment_part:
        if allocation.comment:
            allocation.comment = allocation.comment + comment_part
        else:
            allocation.comment = comment_part
        allocation.save()

    remark_part = field(line, 82, 150)
    if remark_part:
        new_remark = models.Remark()
        new_remark.allocation = allocation
        new_remark.field = remark_part.split(",")[0]
        new_remark.value = ",".join(remark_part.split(",")[1:])
        new_remark.save()


def parse_record(record, last_allocation):
    next_line = record.pop(0)

    # This is sort of ugly, but there are records where the remarks continue onto the next page and we need special
    # handling of these.
    if next_line.startswith("   ") and len(next_line) > 82 and next_line[81] == "*":
        app.logger.info(f"This is a continuation record, parsing as additional comments/remarks on {last_allocation.serial}.")
        # The first line is a comments/remarks line, so it's actually carry-over from the previous page.
        # First we need to get the record these belong to.
        parse_comment_line(next_line, last_allocation)
        while len(record) > 0:
            next_line = record.pop(0)
            parse_comment_line(next_line, last_allocation)

        # and done, end of processing on this partial record. Return the record we were actually working on.
        return last_allocation

    # The first line should be the basic fields for the record
    allocation = parse_record_begin(next_line)

    # Now get any continuation lines for repeating fields. We can identify these because they start with a lot of
    # spaces (early fields can't be repeated)
    next_line = record.pop(0)
    while next_line.startswith("        "):
        parse_allocation_continuation(next_line, allocation)
        next_line = record.pop(0)

    # Now we should have site records. There are at least two of these, one for transmit and the other for receive.
    parse_site(next_line, allocation, 'tx')
    next_line = record.pop(0)
    # As long as we don't find the asterisk separator for comments we know it's still a site.
    while len(next_line) < 82 or next_line[81] != "*":
        parse_site(next_line, allocation, 'rx')
        next_line = record.pop(0)

    # And the remaining lines will be comments/remarks
    while len(record):
        parse_comment_line(next_line, allocation)
        next_line = record.pop(0)

    return allocation

def records_from_page(page_file):
    page_file_f = open(page_file)

    # Find top of page, in case there are spurious blank lines before
    line = page_file_f.readline()
    while "U N C L A S S I F I E D" not in line:
        line = page_file_f.readline()

    # Skip the first six lines, they're all header content
    for i in range(5):
        page_file_f.readline()

    this_record = []
    while True:
        line = page_file_f.readline()
        if "*********" in line:
            # Line of asterisks indicates end of record
            if len(this_record) > 0:
                yield this_record
            this_record = []
        elif line == "\n":
            # Ignore empty lines
            pass
        elif "U N C L A S S I F I E D" in line:
            # For unclear reasons page files sometimes have other pages trailing them but garbled, so if we detect
            # the page footer we stop parsing there so we don't get hung up on that.
            break
        elif line == "":
            # Empty string indicates end of file (unlikely to hit this due to above)
            break
        else:
            this_record.append(line.rstrip())


def pdf_to_pages(pdf_path, pages_path):
    """Extract pages of PDF into a directory of text files, one for each page."""
    fp = open(pdf_path, 'rb')
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    # Process each page contained in the document.
    page_num = 1

    for page in PDFPage.get_pages(fp):
        print(f"Writing page {page_num}...")
        page_num += 1
        interpreter.process_page(page)
        with open(pages_path / f"{page_num}.txt", 'w') as outfile:
            outfile.write(retstr.getvalue())
        retstr.seek(0)


@app.cli.command()
def init_db():
    models.db.create_tables([models.Allocation, models.Emission, models.Site, models.Nts, models.Remark, models.Aus])
    print(f"Spatialite setup...")
    models.db.execute_sql("SELECT InitSpatialMetaData();")
    models.db.execute_sql("SELECT AddGeometryColumn('site', 'Geometry', 4326, 'POINT', 'XY')")
    print(f"Initialized database at {app.config['DB_PATH']}")


@app.cli.command()
def extract_pages():
    print(f"Extracting pages from {app.config['PDF_PATH']} to {app.config['PAGES_DIR_PATH']}")
    pages_path = Path(app.config["PAGES_DIR_PATH"])
    if not pages_path.is_dir():
        pages_path.mkdir()
    print(f"Reading PDF, this will take a while...")
    pdf_to_pages(Path(app.config["PDF_PATH"]), pages_path)


@app.cli.command()
def parse_pages():
    print(f"Reading all page files in {app.config['PAGES_DIR_PATH']}")
    page_paths = Path(app.config["PAGES_DIR_PATH"]).iterdir()
    page_paths = natsorted(page_paths, key=str)

    # We need to pass parse_record the *previous* record parsed each time, it case it finds a continuation page.
    # The first time we'll give it None, since it should never be a continuation.
    allocation = None

    for page_path in page_paths:
        app.logger.info(f"Parsing {page_path}...")
        for record in records_from_page(page_path):
            try:
                # We set allocation to the record we just parsed, in case next time around is a continuation
                allocation = parse_record(record, allocation)
            except Exception as e:
                app.logger.exception(f"Failed to parse {page_path}")
                raise e
