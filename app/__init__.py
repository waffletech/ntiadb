from flask import Flask
import toml
from pathlib import Path
from flask_assets import Environment, Bundle

app = Flask(__name__, instance_relative_config=True)

app.config.from_file("config.toml", load=toml.load)
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True
app.logger.setLevel(app.config['LOG_LEVEL'])

assets = Environment(app)
assets.url = app.static_url_path
assets.directory = app.static_folder
assets.append_path(app.static_folder + '/scss')
scss = Bundle('style.scss', filters='libsass',
              output='css/style.css', depends="*.scss")
assets.register('styles', scss)

from app import models
models.db.init(Path(app.config['DB_PATH']))
from app import loader
from app import main_views