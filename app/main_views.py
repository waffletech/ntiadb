from flask import render_template, request, send_file, make_response, abort, url_for, session
import peewee
import math

from app import models, app


@app.route("/")
def landing_page():
    return make_response(render_template('landing.html'))


@app.route("/allocation/<int:id>")
def allocation(id):
    try:
        allocation = models.Allocation.get(id=id)
    except peewee.DoesNotExist:
        abort(404)

    prefill = render_template('partials/allocation.html', allocation=allocation)
    return make_response(render_template('master.html', prefill=prefill))


@app.route("/allocation/search")
def allocation_query():
    allocations = models.Allocation.select()

    # Pagination
    page_count = math.ceil(allocations.count() / 50)
    if request.args.get("page"):
        try:
            page_num = int(request.args.get("page"))
        except ValueError:
            page_num = 0
        if page_num < 0:
            page_num = 0
    else:
        page_num = 0

    allocations = allocations.offset(50 * page_num).limit(50)

    prefill = render_template('partials/results_list.html', allocations=allocations, pages=range(1, page_count+1))
    return make_response(render_template('master.html', prefill=prefill))
