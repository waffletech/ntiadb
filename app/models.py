import datetime
from peewee import *

db = SqliteDatabase(None, field_types={'point': 'POINT'})
db.load_extension('mod_spatialite')


class BaseModel(Model):
    class Meta:
        database = db


class Allocation(BaseModel):
    """
    Allocation. Allocations are for a specific frequency, so multi-frequency
    systems like repeaters will have more than one allocation.
    """
    cdd       = TextField(null=True)  # Unknown field
    bin       = TextField(null=True)  # Unknown field
    serial    = TextField()           # Serial number
    frequency = IntegerField()        # Frequency, normalized to Hz
    mbr       = TextField(null=True)  # Unknown field
    foi       = TextField(null=True)  # Unknown field
    bureau    = TextField(null=True)  # Bureau. Sometimes an obvious value like "NWS", sometimes less obvious (e.g. "F1" seems to be Forest Service)
    net       = TextField(null=True)  # Network ID, links related stations. not always present. Possibly normalize this into a join table.
    aud       = TextField(null=True)  # Unknown field
    rvd       = TextField(null=True)  # Unknown field
    exd       = TextField(null=True)  # Unknown field
    comment   = TextField(null=True)           # Long free text field on allocations, labeled as "SUP" for some reason

    def display_freq(self):
        frequency = self.frequency
        if frequency / 1000000000 > 1:
            return f"{frequency/1000000000} GHz"
        elif frequency / 1000000 > 1:
            return f"{frequency/1000000} MHz"
        elif frequency / 1000 > 1:
            return f"{frequency/1000} KHz"
        else:
            return f"{frequency} Hz"

    @property
    def tx_site(self):
        return Site.get(Site.allocation == self.id, Site.role == "tx")


class Nts(BaseModel):
    """
    Unknown field that may occur multiple times on an allocation. Listed in
    similar format to emissions but doesn't seem related, since there may be
    more or less of these than there are emissions.
    """
    allocation = ForeignKeyField(Allocation, backref="nts")
    nts        = TextField() # Unknown field, usually in format ANNNNNNN e.g. J1603093, A is always "I" or "J" maybe?


class Aus(BaseModel):
    """
    Unknown field that often occurs multiple times on an allocation. Listed in
    similar format to emissions but doesn't seem related, since there may be
    more or less of these than there are emissions.
    """
    allocation = ForeignKeyField(Allocation, backref="aus")
    aus        = TextField() # Unknown field, usually in format ANNNNNNN e.g. J1603093, A is always "I" or "J" maybe?


class Emission(BaseModel):
    """
    Emission description, with mode and power levels. Often more than one per
    allocation due to multiple modes in use. Sometimes power limits are lower
    for some modes than others.
    """
    allocation    = ForeignKeyField(Allocation, backref="emissions")
    station_class = TextField(null=True)    # Various values that seem to be variations on mobile or fixed.
    designator    = TextField(null=True)    # An FCC-type emission designator like 16K00F3E. Maybe worth it to parse these, maybe not.
    power         = FloatField(null=True) # Power, normalized to watts
    def display_power(self):
        power = self.power
        if power / 1000000000 > 1:
            return f"{power/1000000000} GW"
        elif power / 1000000 > 1:
            return f"{power/1000000} MW"
        elif power / 1000 > 1:
            return f"{power/1000} KW"
        else:
            return f"{power} W"


class Site(BaseModel):
    """
    Radio site, with location and sometimes other info.
    """
    allocation   = ForeignKeyField(Allocation, backref="sites")
    role         = TextField()            # "rx" or "tx"
    state        = TextField(null=True)   # two-letter state code, usually
    location     = TextField(null=True)   # sometimes a city name, other times a geographical place like a mountain
    name         = TextField(null=True)   # "RC", seems to be a site name. Sometimes blank, sometimes a special term.
                                        # "I,NTIA-U" seems to mean nationwide. "USP" may also be used as state/city in
                                        # that case.
    lat          = FloatField(null=True)  # Latitude, normalized to DDD.D...
    lon          = FloatField(null=True)  # Longitude, normalized to DDD.D...
    cl           = TextField(null=True)   # Unknown field, possibly class? values like "WXK   98"
    antenna      = TextField(null=True)   # Antenna info, often not present, values like "08GDIPOLEARRY01132H0006T"
    polarization = TextField(null=True)   # Antenna polarization, often V/H/T
    azimuth      = TextField(null=True)   # Antenna azimuth, degrees or "ND" for nondirectional, other values mysterious
                                          # "R" for rotating?
    spd          = TextField(null=True)   # Unknown field, usually blank
    tme          = TextField(null=True)   # Unknown field, speculatively time slot?, often "1"
    ici          = TextField(null=True)   # Unknown field, usually blank
    msd          = TextField(null=True)   # Unknown field, usually blank


class Remark(BaseModel):
    """
    Remarks are semi-structured "extra" fields that occur on allocations. It
    looks like these are free-text in the implementation, but usually used as
    comma-separated strings where the first part is a three-character type.
    """
    allocation = ForeignKeyField(Allocation, backref="remarks")
    field      = TextField() # Type of field, e.g. "RAD" or "EQT"
    value      = TextField() # Value in field, e.g. "19 MI ESE OF LEWISTON"

    ### Table of known remarks fields:
    # RAD Unknown, e.g. "0030,R"
    # EQT Unknown, e.g. "C,ARMG300,PD620"
    # MFI System name? values like "Weather" or "Natural Resources"
    # AGN Some kind of extra location data, like "19 MI ESE OF LEWISTON"
    # IFI System name? e.g. "NOAA WEATHER RADIO"
    # POC Point of contact, in format NAME,PHONENUMBER,??? (last part might be an allocation ID where the POC was first used?)
    # AGN Note? Sometimes more than one per allocation, content varies. Sometimes a code like "CRMO160151", other times provides another POC
